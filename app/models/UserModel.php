<?php

class UserModel implements ModelInterface {

    public static function getAll() {
        $SQL = 'SELECT * FROM user ORDER BY user_id;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    public static function getById($user_id) {
        $user_id = intval($user_id);
        $SQL = 'SELECT * FROM user WHERE user_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$user_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    public static function getByUsernameAndPass($username, $password) {
        $SQL = 'SELECT * FROM `user` WHERE username = ? AND password = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$username, $password]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    public static function addUser($email, $username, $passwordHash) {
        $SQL = 'INSERT INTO user(username, email, password) VALUES (?, ?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$username, $email, $passwordHash]);
    }
    
    public static function getAllUserByDateCreated(){
        $SQL = 'SELECT * FROM user ORDER BY date;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

}
