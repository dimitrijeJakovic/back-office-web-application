<?php
class BoController extends Controler{
    function index() {
        AdminController::__pre();
        
        $this->set('user', UserModel::getById(Session::get('user_id')));
    }
    
    function allUsers(){
        $this->set('users', UserModel::getAllUserByDateCreated());
    }
    
    function logout(){
        Session::clear();
        Misc::redirect('login');
    }
}
