<?php

class MainController extends Controler {

    function index() {
//        $password = '123';
//        $passwordHash = hash('sha512', $password. Configuration::SALT);
//        
//        dump($passwordHash); exit;
        if ($_POST) {
            $username = filter_input(INPUT_POST, 'username');
            $password = filter_input(INPUT_POST, 'password');

            if (preg_match('/^.{3,}$/', $username) and preg_match('/^.{2,}$/', $password)) {
                $passwordHash = hash('sha512', $password . Configuration::SALT);
                $user = UserModel::getByUsernameAndPass($username, $passwordHash);

                if ($user) {

                    Session::set('user_id', $user->user_id);
                    Session::set('username', $username);
                    Session::set('user_ip', $_SERVER['REMOTE_ADDR']);
                    Session::set('user_agent', $_SERVER['HTTP_USER_AGENT']);

                    Misc::redirect('bo');
                } else {
                    $this->set('massage', 'Nepostojeci korisnik!');
                }
            } else {
                $this->set('massage', 'Neispravna email adresa ili password!');
            }
        }
    }

    function register() {
        if ($_POST) {
            $email = filter_input(INPUT_POST, 'email');
            $username = filter_input(INPUT_POST, 'username');
            $password = filter_input(INPUT_POST, 'password');
            $confirm = filter_input(INPUT_POST, 'confirm');

            if (preg_match('/^.{2,}$/', $email) and 
                preg_match('/^.{2,}$/', $username) and 
                preg_match('/^.{2,}$/', $password) and 
                preg_match('/^.{2,}$/', $confirm)) {
                
                if ($password === $confirm) {
                    
                    $passwordHash = hash('sha512', $password . Configuration::SALT);
                    $user = UserModel::addUser($email, $username, $passwordHash);
                    
                    if ($user) {
                        Misc::redirect('/');
                    } else {
                        $this->set('message', 'Nepostojeci korisnik!');
                    }
                } else {
                    $this->set('message', 'Lozinke moraju da budu iste!');
                }
            } else {
                $this->set('message', 'Neispravna email adresa ili password!');
            }
        }
    }

}
