<?php require_once 'app/views/_global/header.php'; ?>
<?php require_once 'app/views/_global/menu.php'; ?>

<h2>List of all users</h2>

<table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Username</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach ($DATA['users'] as $user): ?>
      <tr>
        <td><?php echo $user->user_id; ?></td>
        <td><?php echo $user->username; ?></td>
        <td><?php echo $user->email; ?></td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php require_once 'app/views/_global/footer.php'; ?>