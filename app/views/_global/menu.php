<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo Configuration::BASE; ?>bo/">Back Office</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="<?php echo Configuration::BASE; ?>bo/">Home</a></li>
            <li><a href="<?php echo Configuration::BASE; ?>bo/allUsers/">List All Users</a></li>
            <li><a href="<?php echo Configuration::BASE; ?>bo/logout">Logout</a></li>
        </ul>
    </div>
</nav>
