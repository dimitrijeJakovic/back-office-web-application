<?php

return[
    [
        'Pattern'    => '|^register/?$|',
        'Controller' => 'Main',
        'Method' => 'register'
    ],
    [
        'Pattern'    => '|^bo/?$|',
        'Controller' => 'Bo',
        'Method' => 'index'
    ],
    [
        'Pattern'    => '|^bo/allUsers/?$|',
        'Controller' => 'Bo',
        'Method' => 'allUsers'
    ],
    
    [
        'Pattern'    => '|^bo/logout/?$|',
        'Controller' => 'Bo',
        'Method' => 'logout'
    ],
    
    
    #osnovna ruta
    [
        'Pattern' => '|^.*$|',
        'Controller' => 'Main',
        'Method' => 'index'
    ]
];
